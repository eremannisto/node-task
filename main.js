const express   = require('express');
const app       = express();
const PORT      = 3000;

// Baseurl: http://localhost:3000
// Endpoint http://localhost:3000/
app.get('/', (req, res) => {
    res.send('Hello World!');
});


/**
 * This is an arrow function that adds two numbers together
 * @param {Number} a - The first number
 * @param {Number} b - The second number
 * @returns {Number} The sum of the two numbers  
 */
const add = (a, b) => {
    return a + b;
}

// Adding endpoint: http://localhost:3000/add?a=value&b=value
app.get('/add', (req, res) => {
    const a = parseInt(req.query.a);
    const b = parseInt(req.query.b);
    const sum = add(a, b);
    res.send(`The sum of ${a} and ${b} is ${sum}`);
});

// App listening on port 3000
app.listen(PORT, () => {
    console.log(`Example app listening at http://localhost:${PORT}`);
});

console.log("Server side program starting...");

